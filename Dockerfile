FROM bubblemelon/pygame-x11

WORKDIR /usr/src/app/sdc

COPY . .

RUN pip3 install -U matplotlib numpy pandas

CMD python3 src/model/TrainingOrtho.py
