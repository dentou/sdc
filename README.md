# Self Driving Car
This repo presents a simulation of neural net based self-driving car, trained with evolutionary algorithms.

## Installation and running
### Local
1. Install python 3
2. Install [pygame](https://www.pygame.org/)
3. Install matplotlib numpy pandas: `pip3 install -U matplotlib numpy pandas`
4. Run: python3 src/model/TrainingOrtho.py

### With docker 
- Option 1: Using docker image on [Docker Hub](https://hub.docker.com/r/dentou/sdc) (recommended)
```
docker pull dentou/sdc
xhost +
docker run -it --rm -e DISPLAY=unix$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix dentou/sdc
xhost -
```
- Option 2: Build and run locally
```
chmod +x docker.sh
./docker.sh
```